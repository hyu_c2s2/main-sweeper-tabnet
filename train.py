import argparse
import itertools
import os
from typing import Tuple

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from sklearn.metrics import roc_auc_score

from pytorch_tabnet.tab_model import TabNetClassifier
import torch

from models import *


np.random.seed(0)


def read_data(dataset_name: str, sample=False, answer_only=False) -> Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]:
    pages = get_source_list_by_name(dataset_name, answer_only)
    sample = True
    if sample:        
        print("We running in TEST!")
        pages = list(get_source_list_by_name(dataset_name, True))[0:10]    
    print('pages amount', len(pages))
    data = list(map(lambda x: get_single_page_features(str(x.id), False), pages))    
    
    basic_data = list(map(lambda x: x[0], data))
    center_data = list(map(lambda x: x[1], data))
    answer_data = list(map(lambda x: x[2], data))
    doc_data = list(map(lambda x: x[3], data))
        
    basic_data_flat = list(itertools.chain(*basic_data))
    center_data_flat = list(itertools.chain(*center_data))
    answer_data_flat = list(itertools.chain(*answer_data))
    doc_data_flat = list(itertools.chain(*doc_data))
    print('Data size', len(basic_data_flat), len(center_data_flat), len(answer_data_flat), len(doc_data_flat))
    
    print(doc_data_flat[:4])
    
    basic_df = basic_features_to_df(basic_data_flat)
    center_df = center_features_to_df(center_data_flat)
    answer_df = tag_to_df(answer_data_flat)
    doc_df = doc_to_df(doc_data_flat)
    
    return basic_df, center_df, answer_df, doc_df


def train_tabnet(train: pd.DataFrame, train_ratio: float, skip_feat=[], base="tabnet", group_page=False, reduce_tags=False):
    remain_ratio = (1 - train_ratio) / 2    
    if group_page:
        num_pages = train['pid'].nunique()
        print(f'grouping in {num_pages}')        
        group_list = np.random.choice(["train", "valid", "test"], p = [train_ratio, remain_ratio, remain_ratio], size=(num_pages,))
        pairs = [ [pid, group_list[i]] for i, pid in enumerate(train['pid'].unique()) ]
        print('pairs', len(pairs))
        print(pairs[:10])
        train["Set"] = train.apply(lambda x: [pair[1] for pair in pairs if pair[0] == x.pid][0], axis=1)
    else:
        print(f'grouping in {train.shape[0]}')
        train["Set"] = np.random.choice(["train", "valid", "test"], p = [train_ratio, remain_ratio, remain_ratio], size=(train.shape[0],))
    
    train_indices = train[train.Set=="train"].index
    valid_indices = train[train.Set=="valid"].index
    test_indices = train[train.Set=="test"].index
    
    target = 'targetTagType'
    
    train['targetTagType'] = train['tagType'].map(lambda x: 1 if 'maincontent' in x else 0)
    
    unused_feat = ['hyuIndex', 'Set', 'pid', 'tagType'] + skip_feat    

    features = [ col for col in train.columns if col not in unused_feat+[target]] 
    print("Using Features:", features)
    categorical_columns = ['tagName', 'visible']
    cat_idxs = [ i for i, f in enumerate(features) if f in categorical_columns]
    # CAUTION: cat_dims[0] (htmltag type) can be changed by custom tags
    num_tags = max_num_of_standard_tags() if reduce_tags else max_num_of_tags()     
    
    cat_dims = [num_tags, 2]
    print('tag reduced', reduce_tags, num_tags)
    
    clf = TabNetClassifier(cat_idxs=cat_idxs,
                       cat_dims=cat_dims,
                       cat_emb_dim=1,
                       optimizer_fn=torch.optim.Adam,
                       optimizer_params=dict(lr=2e-2),
                       scheduler_params={"step_size":50, # how to use learning rate scheduler
                                         "gamma":0.9},
                       scheduler_fn=torch.optim.lr_scheduler.StepLR,
                       mask_type='entmax' # "sparsemax"
                      )
    
    X_train = train[features].values[train_indices]
    y_train = train[target].values[train_indices]

    X_valid = train[features].values[valid_indices]
    y_valid = train[target].values[valid_indices]

    X_test = train[features].values[test_indices]
    y_test = train[target].values[test_indices]
    
    # running in test
    max_epochs = 1000 if not os.getenv("CI", False) else 2
    
    clf.fit(
        X_train=X_train, y_train=y_train,
        eval_set=[(X_train, y_train), (X_valid, y_valid)],
        eval_name=['train', 'valid'],
        eval_metric=['auc'],
        max_epochs=max_epochs , patience=20,
        batch_size=1024, virtual_batch_size=128,
        num_workers=0,
        weights=1,
        drop_last=False
    ) 
    
    plt.plot(clf.history['loss'])
    plt.savefig(f'{base}/loss.png')
    plt.plot(clf.history['train_auc'])
    plt.savefig(f'{base}/train_auc.png')
    plt.plot(clf.history['valid_auc'])
    plt.savefig(f'{base}/valid_auc.png')
    plt.plot(clf.history['lr'])
    plt.savefig(f'{base}/lr.png')
    
    saving_path_name = f"{base}/tabnet_model"
    saved_filepath = clf.save_model(saving_path_name)    
    
    preds = clf.predict_proba(X_test)
    test_auc = roc_auc_score(y_score=preds[:,1], y_true=y_test)


    preds_valid = clf.predict_proba(X_valid)
    valid_auc = roc_auc_score(y_score=preds_valid[:,1], y_true=y_valid)
    
    summary_file = open(f'{base}/summary.txt', 'w')
    summary_file.write(f"BEST VALID SCORE: {clf.best_cost}\nFINAL TEST SCORE: {test_auc}")
    
    comment_file = open(f'{base}/comment.txt', 'w')
    comment_file.write(f'Used Html Tag Option: {num_tags}')

    print(f"BEST VALID SCORE: {clf.best_cost}")
    print(f"FINAL TEST SCORE: {test_auc}")
    

def main(args):
    print(args)
    dataset_name = args.dataset    
    train_ratio = int(args.train_ratio) / 100        
    modes = args.feature.split(',')
    accept_modes = ['basic', 'center', 'center2', 'both', 'both2', 'basicanswer', 'bothanswer', 'both2answer']
    accepted_modes = list(filter(lambda x: x in accept_modes, modes))
    reduce_tags = args.reduce
    group_page = args.group
    
    if(len(accepted_modes) == 0):
        print("No Acceptable Modes in", modes)        
        return
    
    running_modes = []
    if group_page == '0':
        running_modes = [ [x, False] for x in accepted_modes ]        
    elif group_page == '2':
        running_modes = [ [x, True] for x in accepted_modes ] + [ [x, False] for x in accepted_modes ]
    else:
        running_modes = [ [x, True] for x in accepted_modes ]        
    
    print("Run modes", running_modes, group_page)       
    
    
    # test mode    
    basic_df, center_df, answer_df, doc_df = read_data(dataset_name, False, True)    
    
    df = pd.DataFrame()            
    df = pd.concat([basic_df, center_df, answer_df, doc_df], axis=1)
    
    df['isNav'] = df['tagType'].map(lambda x: 1 if 'nav' in x else 0)
    df['isTitle'] = df['tagType'].map(lambda x: 1 if 'title' in x else 0)
    answer_feat = ['isNav', 'isTitle']
    
    print("Before", df.head(20))
    
    if reduce_tags:
        df["tagName"] = df["tagName"].map(get_standard_num)
        
    print("After", df.head(20), df['tagName'].nunique())
    
    for [mode, group_page] in running_modes:
        grouped = '-group' if group_page else ''
        standard_tag_only = '-standardtag' if reduce_tags else ''
        dataset_name = dataset_name if dataset_name != 'all' else 'GoogleTrends-all'
        base = f'./result/{dataset_name}-{mode}-{int(train_ratio*100)}{grouped}{standard_tag_only}-docsize'        
        os.makedirs(base)
        print("Run with:", dataset_name, mode, train_ratio, group_page)
        print("Result -->", base)
        
        skip_feat = []
        center_feat = ['c0', 'c1', 'c2', 'c3', 'c4']
        center2_feat = ['c1', 'c2', 'c3']
        if mode == 'basic':
            skip_feat = answer_feat + ['c0', 'c1', 'c2', 'c3', 'c4']
        elif mode == 'center':
            skip_feat = answer_feat+ ['tagName', 'descendants', 'anchorDescendants', 'textLength', 'visible', 'offsetTop', 'offsetLeft', 'left', 'top', 'right', 'bottom', 'width', 'height']
        elif mode == 'center2':
            skip_feat = answer_feat+['tagName', 'descendants', 'anchorDescendants', 'textLength', 'visible', 'offsetTop', 'offsetLeft', 'left', 'top', 'right', 'bottom', 'width', 'height', 'c1', 'c2', 'c3']
        elif mode == 'both':
            skip_feat = answer_feat
        elif mode == 'both2':
            skip_feat = answer_feat+['c1', 'c2', 'c3']        
        elif mode == 'basicanswer':
            skip_feat = ['c0', 'c1', 'c2', 'c3', 'c4']
        elif mode == 'both2answer':
            skip_feat = ['c1', 'c2', 'c3']
            
    
        train_tabnet(df, train_ratio, skip_feat, base, group_page, reduce_tags)    
        print(f"Done {dataset_name}-{mode}-{int(train_ratio*100)}")
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('dataset', help='dataset name')
    parser.add_argument('train_ratio', nargs='?', default='80', help='train valid test ratio')
    parser.add_argument('-f', '--feature', nargs='?', default='basic', help='basic centers center2 both both2')    
    parser.add_argument('-g', '--group', nargs='?', default='1', help='train/valid/test set grouped by document, not element')
    parser.add_argument('-r', '--reduce', action='store_true', help='html tags category uses only standard tags')    
    args = parser.parse_args()
    main(args)