from mongoengine import *
import pandas as pd
import itertools

connect(host="mongodb://root:1234@mongo:27017/mainContentFramework?authSource=admin")


class HtmlTag(Document):
    num = IntField()
    name = StringField()
    standardNum = IntField()
    mdnCategory = StringField()
    mdnCategoryIndex = IntField()
    mdnTagIndex = IntField()
    meta = {
        'collection': 'htmlTag'
    }
    
    
class ElementFeatures(Document):
    pid = ObjectIdField()
    basicFeatures = ListField(FloatField())
    answer = StringField()
    distanceToCenters = ListField(FloatField())
    documentFeatures = ListField(FloatField())
    meta = {
        'collection': 'ElementFeatures'
    }

    
class Webpage(Document):
    url = StringField(required=True)
    source = StringField(required=True)
    listedDate = DateTimeField(required=True)
    savedDate = DateTimeField()
    description = DictField()
    sample = BooleanField()
    meta = {
        'collection': 'Webpage'
    }
    
    
class Answer(Document):
    pid = ObjectIdField()
    userId = StringField()
    tagType = StringField()
    hyuIndex = StringField()
    meta = {
        'collection': 'Answer'
    }
                        
        
def get_source_list_by_name(name: str, answered_only: bool = False):
    if(name == 'all'):
        current_source = ['GoogleTrends-2020', 'GoogleTrends-2017']
        sources = list(map(lambda x: get_source_list_by_name(x, answered_only), current_source))
        return list(itertools.chain(*sources))
    pages = Webpage.objects(source=name, sample=True, savedDate__ne=None)
    if answered_only:
        return list(filter(lambda x: is_page_having_answer(x.id), pages))
    return pages


def get_answers_by_pid(pid: str):
    return Answer.objects(pid=pid)


def is_page_having_answer(pid: str, tag_type: str = 'maincontent'):
    has_maincontent = False
    answers = get_answers_by_pid(pid)
    for answer in answers:   
        if answer.tagType == 'maincontent':
            has_maincontent = True
    return has_maincontent


def get_features_by_pid(pid: str):
    return ElementFeatures.objects(pid=pid)


def get_single_page_features(pid: str, to_df: bool = True):
    features = get_features_by_pid(pid)
    
    single_page_features_basic = [feature.basicFeatures for feature in features]
    single_page_tag = [[pid, feature.answer] for feature in features]
    single_page_features_center = [feature.distanceToCenters for feature in features]
    single_page_features_document = [features[0].documentFeatures] * len(features) # features[0] always has data?
    assert len(features[0].documentFeatures) > 0, 'document size cannot read'    
        
    if(to_df):
        return pd.DataFrame(single_page_features_basic, columns=['hyuIndex', 'tagName', 'descendants', 'anchorDescendants', 'textLength', 'visible', 'offsetTop', 'offsetLeft', 'left', 'top', 'right', 'bottom', 'width', 'height']), \
            pd.DataFrame(single_page_features_center, columns=['c0', 'c1', 'c2', 'c3', 'c4']), \
            pd.DataFrame(single_page_tag, columns=['pid', 'tagType']), \
            pd.DataFrame(single_page_features_document, columns=['screen_width', 'screen_height', 'doc_width', 'doc_height'])
    
    else:
        return single_page_features_basic, single_page_features_center, single_page_tag, single_page_features_document
    
    
def basic_features_to_df(data) -> pd.DataFrame:
    return pd.DataFrame(data, columns=['hyuIndex', 'tagName', 'descendants', 'anchorDescendants', 'textLength', 'visible', 'offsetTop', 'offsetLeft', 'left', 'top', 'right', 'bottom', 'width', 'height'])


def center_features_to_df(data) -> pd.DataFrame:
    return pd.DataFrame(data, columns=['c0', 'c1', 'c2', 'c3', 'c4'])


def tag_to_df(data) -> pd.DataFrame:
    return pd.DataFrame(data, columns=['pid', 'tagType'])


def doc_to_df(data) -> pd.DataFrame:
    return pd.DataFrame(data, columns=['screen_width', 'screen_height', 'doc_width', 'doc_height'])


def get_tag_name(num: int):
    return HtmlTag.objects(num=num).first().name


def max_num_of_tags():
    snums = list(map(lambda x: x.num, HtmlTag.objects()))
    return len(snums)


def max_num_of_standard_tags():
    # https://developer.mozilla.org/en-US/docs/Web/HTML/Element at 2021-09-17 + Unknown Tags
    return 138


def get_standard_num(num: int):    
    return float(HtmlTag.objects(num=num)[0].mdnTagIndex)

    
def get_standard_category(num: int):
    return float(HtmlTag.objects(num=num)[0].mdnCategoryIndex)
        
